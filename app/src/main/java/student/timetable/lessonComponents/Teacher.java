package student.timetable.lessonComponents;

public class Teacher extends Component{
    private String name;

    public Teacher(String name) {
        this.name = name;
    }

    public String getName(){
      return this.name;
    }

    public void setName(String newName){
      this.name = newName;
    }
}
