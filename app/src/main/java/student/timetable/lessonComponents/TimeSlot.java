package student.timetable.lessonComponents;

import java.sql.Time;

import student.timetable.carriage.exceptions.IncorrectTimeSlotComparisonException;
import student.timetable.carriage.exceptions.IncorrectTimeSlotParametersException;

public class TimeSlot extends Component{
    private Time timeStart;
    private Time timeEnd;

    public Time getTimeStart(){
      return this.timeStart;
    }

    public Time getTimeEnd(){
      return this.timeEnd;
    }

    public TimeSlot(Time timeStart, Time timeEnd) throws IncorrectTimeSlotParametersException {
      if (timeStart.compareTo(timeEnd) >= 0){
        throw new IncorrectTimeSlotParametersException("IncorrectTimeSlotParametersException");
      } else {
        this.timeStart = timeStart;
        this.timeEnd = timeEnd;
      }
    }

    public int compareTo(TimeSlot timeslot) throws IncorrectTimeSlotComparisonException {
      if ((this.timeStart.compareTo(timeslot.timeStart) == -1)||(this.timeEnd.compareTo(timeslot.timeEnd) == -1)){
        return -1;
      }
      if ((this.timeStart.compareTo(timeslot.timeStart) == 1)||(this.timeEnd.compareTo(timeslot.timeEnd) == 1)){
        return 1;
      }
      if ((this.timeStart.compareTo(timeslot.timeStart) == 0)||(this.timeEnd.compareTo(timeslot.timeEnd) == 0)){
        return 0;
      }
      throw new IncorrectTimeSlotComparisonException("IncorrectTimeSlotComparisonException");
    }

    public void setTimeBorders(Time timeStart, Time timeEnd){
      this.timeStart = timeStart;
      this.timeEnd = timeEnd;
    }
  }
