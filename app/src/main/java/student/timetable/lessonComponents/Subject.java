package student.timetable.lessonComponents;

public class Subject extends Component{
    private String name;

    public Subject(String name) {
        this.name = name;
    }

    public String getName(){
      return this.name;
    }

    public void setName(String newName){
      this.name = newName;
    }
}
