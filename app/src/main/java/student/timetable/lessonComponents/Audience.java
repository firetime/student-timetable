package student.timetable.lessonComponents;

public class Audience extends Component{
    private String cabinet;

    public Audience(String cabinet) {
        this.cabinet = cabinet;
    }

    public String getCabinet(){
      return this.cabinet;
    }

    public void setCabinet(String newCab){
      this.cabinet = newCab;
    }
}
