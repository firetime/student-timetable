package student.timetable.lessonComponents;

public class Group extends Component{
    private String group;

    public Group(String group) {
        this.group = group;
    }

    public String getGroup(){
      return this.group;
    }

    public void setGroup(String newGroup){
      this.group = newGroup;
    }
}
