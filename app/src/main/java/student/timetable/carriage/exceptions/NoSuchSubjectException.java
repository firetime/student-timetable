package student.timetable.carriage.exceptions;

public class NoSuchSubjectException extends Exception{

    String message;

    public NoSuchSubjectException(String message) {
        super(message);
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

}
