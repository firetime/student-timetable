package student.timetable.carriage.exceptions;

public class NoSuchGroupException extends Exception{

    String message;

    public NoSuchGroupException(String message) {
        super(message);
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

}
