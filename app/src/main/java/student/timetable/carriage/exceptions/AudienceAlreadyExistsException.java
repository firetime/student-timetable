package student.timetable.carriage.exceptions;

public class AudienceAlreadyExistsException extends Exception{
    public AudienceAlreadyExistsException(String message) {
        super(message);
    }
}
