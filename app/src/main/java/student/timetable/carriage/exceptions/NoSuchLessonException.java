package student.timetable.carriage.exceptions;

public class NoSuchLessonException extends Exception{

    String message;

    public NoSuchLessonException(String message) {
        super(message);
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

}
