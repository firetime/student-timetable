package student.timetable.carriage.exceptions;

public class SubjectAlreadyExistsException extends Exception{

    String message;

    public SubjectAlreadyExistsException(String message) {
        super(message);
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

}
