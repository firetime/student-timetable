package student.timetable.carriage.exceptions;

public class TeacherAlreadyExistsException extends Exception{

    String message;

    public TeacherAlreadyExistsException(String message) {
        super(message);
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

}