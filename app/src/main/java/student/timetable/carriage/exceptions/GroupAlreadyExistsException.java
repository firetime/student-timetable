package student.timetable.carriage.exceptions;

public class GroupAlreadyExistsException extends Exception{

    String message;

    public GroupAlreadyExistsException(String message) {
        super(message);
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

}