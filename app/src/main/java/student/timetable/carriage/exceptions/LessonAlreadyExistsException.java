package student.timetable.carriage.exceptions;

public class LessonAlreadyExistsException extends Exception{

    String message;

    public LessonAlreadyExistsException(String message) {
        super(message);
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

}
