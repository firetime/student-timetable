package student.timetable.carriage.exceptions;

public class NoSuchAudienceException extends Exception{

    String message;

    public NoSuchAudienceException(String message) {
        super(message);
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

}
