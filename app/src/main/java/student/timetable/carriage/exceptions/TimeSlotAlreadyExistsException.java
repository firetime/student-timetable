package student.timetable.carriage.exceptions;

public class TimeSlotAlreadyExistsException extends Exception{

    String message;

    public TimeSlotAlreadyExistsException(String message) {
        super(message);
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

}