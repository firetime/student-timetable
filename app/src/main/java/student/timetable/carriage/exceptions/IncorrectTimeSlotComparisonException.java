package student.timetable.carriage.exceptions;

public class IncorrectTimeSlotComparisonException extends Exception{

    String message;

    public IncorrectTimeSlotComparisonException(String message) {
        super(message);
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

}
