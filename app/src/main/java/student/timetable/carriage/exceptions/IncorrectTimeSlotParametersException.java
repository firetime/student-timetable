package student.timetable.carriage.exceptions;

public class IncorrectTimeSlotParametersException extends Exception{

    String message;

    public IncorrectTimeSlotParametersException(String message) {
        super(message);
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

}
