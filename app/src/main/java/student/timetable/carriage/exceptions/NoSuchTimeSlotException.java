package student.timetable.carriage.exceptions;

public class NoSuchTimeSlotException extends Exception{

    String message;

    public NoSuchTimeSlotException(String message) {
        super(message);
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

}
