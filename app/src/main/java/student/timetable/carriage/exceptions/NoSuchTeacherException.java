package student.timetable.carriage.exceptions;

public class NoSuchTeacherException extends Exception{

    String message;

    public NoSuchTeacherException(String message) {
        super(message);
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

}
