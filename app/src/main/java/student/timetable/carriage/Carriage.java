package student.timetable.carriage;

import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

import student.timetable.carriage.exceptions.*;
import student.timetable.lessonComponents.*;


public class Carriage{
  public static List<Subject> subjects = new ArrayList<Subject>();
  public static List<Teacher> teachers = new ArrayList<Teacher>();
  public static List<Audience> audiences = new ArrayList<Audience>();
  public static List<Group> groups = new ArrayList<Group>();
  public static List<TimeSlot> timeslots = new ArrayList<TimeSlot>();

  public static void addSubject(Subject subject) throws SubjectAlreadyExistsException{
    if (subjects.contains(subject)) {
      throw new SubjectAlreadyExistsException("SubjectAlreadyExistsException");
    } else {
      subjects.add(subject);
    }
  }

  public static void addTeacher(Teacher teacher) throws TeacherAlreadyExistsException{
    if (teachers.contains(teacher)) {
      throw new TeacherAlreadyExistsException("TeacherAlreadyExistsException");
    } else {
      teachers.add(teacher);
    }
  }

  public static void addAudience(Audience audience) throws AudienceAlreadyExistsException {
    if (audiences.contains(audience)) {
      throw new AudienceAlreadyExistsException("AudienceAlreadyExistsException");
    } else {
      audiences.add(audience);
    }
  }

  public static void addGroup(Group group) throws GroupAlreadyExistsException {
    if (groups.contains(group)) {
      throw new GroupAlreadyExistsException("GroupAlreadyExistsException");
    } else {
      groups.add(group);
    }
  }

  public static void addTimeSlot(TimeSlot timeslot) throws TimeSlotAlreadyExistsException {
    if (timeslots.contains(timeslot)) {
      throw new TimeSlotAlreadyExistsException("TimeSlotAlreadyExistsException");
    } else {
      timeslots.add(timeslot);
    }
  }

  public static void removeSubject(Subject subject) throws NoSuchSubjectException {
    if (!(subjects.contains(subject))) {
      throw new NoSuchSubjectException("NoSuchSubjectException");
    } else {
      subjects.remove(subject);
    }
  }

  public static void removeTeacher(Teacher teacher) throws NoSuchTeacherException{
    if (!(teachers.contains(teacher))) {
      throw new NoSuchTeacherException("NoSuchTeacherException");
    } else {
      teachers.remove(teacher);
    }
  }

  public static void removeAudience(Audience audience) throws NoSuchAudienceException{
    if (!(audiences.contains(audience))) {
      throw new NoSuchAudienceException("NoSuchAudienceException");
    } else {
      audiences.remove(audience);
    }
  }

  public static void removeGroup(Group group) throws NoSuchGroupException {
    if (!(groups.contains(group))) {
      throw new NoSuchGroupException("NoSuchGroupException");
    } else {
      groups.remove(group);
    }
  }

  public static void removeTimeSlot(TimeSlot timeslot) throws NoSuchTimeSlotException{
    if (!(timeslots.contains(timeslot))) {
      throw new NoSuchTimeSlotException("NoSuchTimeSlotException");
    } else {
      timeslots.remove(timeslot);
    }
  }

  public static void editSubject(Subject subject, String newName) throws NoSuchSubjectException{
    if (!(subjects.contains(subject))) {
      throw new NoSuchSubjectException("NoSuchSubjectException");
    } else {
      subjects.get(subjects.indexOf(subject)).setName(newName);
    }
  }

  public static void editTeacher(Teacher teacher, String newName) throws NoSuchTeacherException{
    if (!(teachers.contains(teacher))) {
      throw new NoSuchTeacherException("NoSuchTeacherException");
    } else {
      teachers.get(teachers.indexOf(teacher)).setName(newName);
    }
  }

  public static void editAudience(Audience audience, String newCabinet) throws NoSuchAudienceException{
    if (!(audiences.contains(audience))) {
      throw new NoSuchAudienceException("NoSuchAudienceException");
    } else {
      audiences.get(audiences.indexOf(audience)).setCabinet(newCabinet);
    }
  }

  public static void editGroup(Group group, String newGroup) throws NoSuchGroupException{
    if (!(groups.contains(group))) {
      throw new NoSuchGroupException("NoSuchGroupException");
    } else {
      groups.get(groups.indexOf(group)).setGroup(newGroup);
    }
  }

  public static void editTimeSlot(TimeSlot oldTimeslot, Time timeStart, Time timeEnd) throws NoSuchTimeSlotException{
    if (!(timeslots.contains(oldTimeslot))) {
      throw new NoSuchTimeSlotException("NoSuchTimeSlotException");
    } else {
      timeslots.get(timeslots.indexOf(oldTimeslot)).setTimeBorders(timeStart, timeEnd);
    }
  }
}
