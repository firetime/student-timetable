package student.timetable.timetables;

import student.timetable.lesson.Lesson;
import student.timetable.carriage.exceptions.IncorrectTimeSlotComparisonException;
import student.timetable.lessonComponents.Group;

import java.util.List;
import java.util.ArrayList;
import java.util.GregorianCalendar;

public class DayTimeTable {
  public GregorianCalendar date;
  public static ArrayList<Lesson> lessons = new ArrayList<Lesson>();

  DayTimeTable(GregorianCalendar date){
    this.date = date;
  }

  public boolean addLesson(Lesson lesson){
    for(int i = 0; i < this.lessons.size(); i++){
      Lesson check = this.lessons.get(i);
      if (check.group == lesson.group){
        if (check.timeSlot.getTimeStart().compareTo(lesson.timeSlot.getTimeStart()) == 1 && check.timeSlot.getTimeEnd().compareTo(lesson.timeSlot.getTimeEnd()) == -1){
          System.out.println("Некорректное время: время у группы занято");
          return false;
        }
        if (check.timeSlot.getTimeStart().compareTo(lesson.timeSlot.getTimeStart()) == -1 && check.timeSlot.getTimeEnd().compareTo(lesson.timeSlot.getTimeEnd()) == 1){
          System.out.println("Некорректное время: время у группы занято");
          return false;
        }
      }
      if (check.teacher == lesson.teacher){
        if (check.timeSlot.getTimeStart().compareTo(lesson.timeSlot.getTimeStart()) == 1 && check.timeSlot.getTimeEnd().compareTo(lesson.timeSlot.getTimeEnd()) == -1){
          System.out.println("Некорректное время: учитель у группы занято");
          return false;
        }
        if (check.timeSlot.getTimeStart().compareTo(lesson.timeSlot.getTimeStart()) == -1 && check.timeSlot.getTimeEnd().compareTo(lesson.timeSlot.getTimeEnd()) == 1){
          System.out.println("Некорректное время: учитель у группы занято");
          return false;
        }
      }
      if (check.audience == lesson.audience){
        if (check.timeSlot.getTimeStart().compareTo(lesson.timeSlot.getTimeStart()) == 1 && check.timeSlot.getTimeEnd().compareTo(lesson.timeSlot.getTimeEnd()) == -1){
          System.out.println("Некорректное время: аудитория у группы занято");
          return false;
        }
        if (check.timeSlot.getTimeStart().compareTo(lesson.timeSlot.getTimeStart()) == -1 && check.timeSlot.getTimeEnd().compareTo(lesson.timeSlot.getTimeEnd()) == 1){
          System.out.println("Некорректное аудитория: время у группы занято");
          return false;
        }
      }
    }
    this.lessons.add(lesson);
    return true;
  }

  public boolean removeLesson(Lesson lesson){
    for (int i = 0; i < this.lessons.size(); i++) {
      if (this.lessons.get(i).equals(lesson)){
        this.lessons.set(i, null);
        return true;
      }
    }
    return false;
  }

  public boolean editLesson(Lesson newLesson, Lesson oldLesson){
    for (int i = 0; i < this.lessons.size(); i++) {
      if (lessons.get(i).equals(oldLesson)){
        lessons.set(i, newLesson);
        return true;
      }
    }
    return false;
  }

  public List<List<Lesson>> getDayTimeTable(){
    List<List<Lesson>> groupsLessons = new ArrayList<List<Lesson>>();
    List<Group> visitedGroups = new ArrayList<Group>();
    for (int i = 0; i < this.lessons.size(); i++) {
      Lesson currentLesson = this.lessons.get(i);
      if (visitedGroups.contains(currentLesson.group)) {
        groupsLessons.get(visitedGroups.indexOf(currentLesson.group)).add(currentLesson);
      } else {
        visitedGroups.add(currentLesson.group);
        groupsLessons.add(new ArrayList<Lesson>());
        groupsLessons.get(visitedGroups.indexOf(currentLesson.group)).add(currentLesson);
      }
    }
    for (int i = 0; i < groupsLessons.size(); i++) {
      for (int j = 0; j < groupsLessons.get(i).size() - 1; j++) {
        int minInd = 0;
        for (int h = j + 1; h < groupsLessons.get(i).size(); h++) {
          try{
            if (groupsLessons.get(i).get(h).timeSlot.compareTo(groupsLessons.get(i).get(minInd).timeSlot) == -1) {
                Lesson bubble = groupsLessons.get(i).get(minInd);
                groupsLessons.get(i).set(minInd, groupsLessons.get(i).get(h));
                groupsLessons.get(i).set(h, bubble);
            }
          } catch (IncorrectTimeSlotComparisonException e) {
            System.out.println("Некорректные списки TimeSlots");
            return null;
          }
        }
      }
    }
    return groupsLessons;
  }
}
