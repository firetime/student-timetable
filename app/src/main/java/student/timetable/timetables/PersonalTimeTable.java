package student.timetable.timetables;
import student.timetable.carriage.exceptions.IncorrectTimeSlotComparisonException;
import student.timetable.lesson.Lesson;
import student.timetable.lessonComponents.Component;

import java.util.List;
import java.util.ArrayList;

public class PersonalTimeTable<T extends Component>{
  public T forWho;
  public List<List<Lesson>> lessons;


  PersonalTimeTable(T forWho){
    this.forWho = forWho;
    this.getPersonalTimeTable();
  }

  public void getPersonalTimeTable(){
    List<List<Lesson>> daysLessons = new ArrayList<List<Lesson>>();
    List<DayTimeTable> visitedDays = new ArrayList<DayTimeTable>();
    for (int i = 0; i < TimeTable.dayTimeTables.size(); i++) {
      DayTimeTable currentDay = TimeTable.dayTimeTables.get(i);
      for (int j = 0; j < currentDay.lessons.size(); j++) {
        Lesson currentLesson = currentDay.lessons.get(j);
        if ((currentLesson.group == forWho) || (currentLesson.teacher == forWho) || (currentLesson.audience == forWho)) {
          if (visitedDays.contains(currentDay)) {
            daysLessons.get(i).add(currentLesson);
          } else {
            visitedDays.add(currentDay);
            daysLessons.add(new ArrayList<Lesson>());
            daysLessons.get(i).add(currentLesson);
          }
        }
      }
    }
    for (int i = 0; i < daysLessons.size(); i++) {
      for (int j = 0; j < daysLessons.get(i).size() - 1; j++) {
        int minInd = 0;
        for (int h = j + 1; h < daysLessons.get(i).size(); h++) {
          try{
            if (daysLessons.get(i).get(h).timeSlot.compareTo(daysLessons.get(i).get(minInd).timeSlot) == -1) {
                Lesson bubble = daysLessons.get(i).get(minInd);
                daysLessons.get(i).set(minInd, daysLessons.get(i).get(h));
                daysLessons.get(i).set(h, bubble);
            }
          } catch (IncorrectTimeSlotComparisonException e) {
            System.out.println("Некорректные списки TimeSlots");
            return;
          }
        }
      }
    }
    this.lessons = daysLessons;
  }
}
