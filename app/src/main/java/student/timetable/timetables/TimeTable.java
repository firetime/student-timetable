package student.timetable.timetables;
import student.timetable.carriage.Carriage;
import student.timetable.lesson.Lesson;
import student.timetable.carriage.exceptions.NoSuchTimeSlotException;
import student.timetable.lessonComponents.*;
import java.util.List;
import java.util.ArrayList;
import java.util.GregorianCalendar;



public class TimeTable {
    public static ArrayList<DayTimeTable> dayTimeTables;
    public static int daysQuan;

    // public TimeTable(){}

    public static boolean addLesson(Subject subject, Teacher teacher,
                                    Audience audience, Group group, TimeSlot timeSlot, GregorianCalendar date) throws NoSuchTimeSlotException {
      if (!(Carriage.timeslots.contains(timeSlot))){
        throw new NoSuchTimeSlotException("NoSuchTimeSlotException");
      }
      Lesson lesson = new Lesson(timeSlot, group, audience, teacher, subject);
      for(int i = 0; i < TimeTable.dayTimeTables.size(); i++){
        DayTimeTable curr = TimeTable.dayTimeTables.get(i);
        if (curr.date == date) {
          return curr.addLesson(lesson);
        }
      }
      DayTimeTable newDay = new DayTimeTable(date);
      if (newDay.addLesson(lesson)){
        return TimeTable.addDayTimeTable(date);
      } else {
        return false;
      }
    }

    public static boolean removeLesson(Subject subject, Teacher teacher,
                             Audience audience, Group group, TimeSlot timeSlot, GregorianCalendar date) throws NoSuchTimeSlotException{
      if (!(Carriage.timeslots.contains(timeSlot))){
        throw new NoSuchTimeSlotException("NoSuchTimeSlotException");
      }
      Lesson lesson = new Lesson(timeSlot, group, audience, teacher, subject);
      for(int i = 0; i < dayTimeTables.size(); i++){
        DayTimeTable curr = dayTimeTables.get(i);
        if (curr.date == date) {
          return curr.removeLesson(lesson);
        }
      }
      return false;
    }

    public static boolean editLesson(Subject newSubject, Teacher newTeacher,
                             Audience newAudience, Group newGroup, TimeSlot newTimeSlot,
                                    Subject oldSubject, Teacher oldTeacher,
                            Audience oldAudience, Group oldGroup, TimeSlot oldTimeSlot, GregorianCalendar date){
      Lesson newLesson = new Lesson(newTimeSlot, newGroup, newAudience, newTeacher, newSubject);
      Lesson oldLesson = new Lesson(oldTimeSlot, oldGroup, oldAudience, oldTeacher, oldSubject);
      for(int i = 0; i < dayTimeTables.size(); i++){
        DayTimeTable curr = dayTimeTables.get(i);
        if (curr.date == date) {
          return curr.editLesson(newLesson, oldLesson);
        }
      }
      return false;
    }

    public static boolean addDayTimeTable(GregorianCalendar date){
      DayTimeTable newDay = new DayTimeTable(date);
      return TimeTable.dayTimeTables.add(newDay);
    }

    public static boolean addDayTimeTable(DayTimeTable newDay){
      return TimeTable.dayTimeTables.add(newDay);
    }

    public static List<List<Lesson>> getDayTimeTable(GregorianCalendar date){
      for(int i = 0; i < TimeTable.dayTimeTables.size(); i++){
        DayTimeTable curr = TimeTable.dayTimeTables.get(i);
        if (curr.date == date) {
          return curr.getDayTimeTable();
        }
      }
      return null;
    }

    public static List<List<Lesson>> getPersonalTimeTable(Component person){
      if (!(person instanceof Component)) {
        return null;
      }
      if (person instanceof Audience){
        PersonalTimeTable<Audience> timeTable = new PersonalTimeTable<Audience>((Audience)person);
        return timeTable.lessons;
      }
      if (person instanceof Group){
        PersonalTimeTable<Group> timeTable = new PersonalTimeTable<Group>((Group)person);
        return timeTable.lessons;
      }
      if (person instanceof Subject){
        PersonalTimeTable<Subject> timeTable = new PersonalTimeTable<Subject>((Subject)person);
        return timeTable.lessons;
      }
      if (person instanceof Teacher){
        PersonalTimeTable<Teacher> timeTable = new PersonalTimeTable<Teacher>((Teacher)person);
        return timeTable.lessons;
      }
      else {
        PersonalTimeTable<TimeSlot> timeTable = new PersonalTimeTable<TimeSlot>((TimeSlot)person);
        return timeTable.lessons;
      }
    }
}
