package student.timetable.menu;

import student.timetable.controllers.*;

public class Menu {
    public Node createMenu() {
        Node node1 = new Node(2);

        Node node21 = new Node(5);
        Node node22 = new Node(new TimeTableController());

        Node node311 = new Node(new LessonController());
        Node node312 = new Node(new GroupController());
        Node node313 = new Node(new SubjectController());
        Node node314 = new Node(new TeacherController());
        Node node315 = new Node(new TimeSlotController());


        node1.setChild(0, node21);
        node1.setChild(1, node22);
        node21.setChild(0, node311);
        node21.setChild(1, node312);
        node21.setChild(2, node313);
        node21.setChild(3, node314);
        node21.setChild(4, node315);

        return node1;
    }
}
