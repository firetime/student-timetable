package student.timetable.menu;

import student.timetable.controllers.GeneralController;

public class Node {
    public Node parent;
    public Node[] children;
    public GeneralController generalController;

    public Node(int n) {
        this.children = new Node[n];
    }

    public Node(GeneralController generalController) {
        this.generalController = generalController;
    }

    public Node getParent() {
        return this.parent;
    }

    public void setParent(Node parent) {
        this.parent = parent;
    }

    public Node[] getChildren() {
        return this.children;
    }

    public void setChild(int i, Node node) {
        if (i >= this.children.length) return;
        this.children[i] = node;
        node.setParent(this);
    }

    public GeneralController getGeneralController() {
        return generalController;
    }

    public void setGeneralController(GeneralController generalController) {
        this.generalController = generalController;
    }

    public boolean hasParent() {
        return this.parent != null;
    }

    public Node goToChild(int n) {
        if (n >= children.length) try {
            throw new Exception();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this.children[n];
    }

    public Node goToParent() {
        return this.parent;
    }

    public void doSmth(int n) throws Exception {
        this.generalController.doSmth(n);
    }
}