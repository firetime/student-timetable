package student.timetable;

import student.timetable.controllers.Controller;
import student.timetable.menu.Menu;
import student.timetable.menu.Node;

public class App {

    public static void main(String[] args) throws Exception {
        Controller controller = new Controller();
        Menu menu = new Menu();
        Node node = menu.createMenu();
        controller.processRequest(node);
    }
}
