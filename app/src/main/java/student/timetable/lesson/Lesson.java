package student.timetable.lesson;

import student.timetable.lessonComponents.*;

public class Lesson {
    public TimeSlot getTimeSlot() {
        return timeSlot;
    }

    public void setTimeSlot(TimeSlot timeSlot) {
        this.timeSlot = timeSlot;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Audience getAudience() {
        return audience;
    }

    public void setAudience(Audience audience) {
        this.audience = audience;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public TimeSlot timeSlot;
    public Group group;
    public Audience audience;
    public Teacher teacher;
    public Subject subject;

    public Lesson(TimeSlot timeSlot, Group group, Audience audience, Teacher teacher, Subject subject) {
        this.timeSlot = timeSlot;
        this.group = group;
        this.audience = audience;
        this.teacher = teacher;
        this.subject = subject;
    }

    public boolean equals(Lesson lesson){
      if (this.timeSlot != lesson.timeSlot){
        return false;
      }
      if (this.group != lesson.group){
        return false;
      }
      if (this.audience != lesson.audience){
        return false;
      }
      if (this.teacher != lesson.teacher){
        return false;
      }
      if (this.subject != lesson.subject){
        return false;
      }
      return true;
    }
  }
