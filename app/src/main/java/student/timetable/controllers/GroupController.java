package student.timetable.controllers;

import student.timetable.carriage.exceptions.GroupAlreadyExistsException;
import student.timetable.carriage.Carriage;
import student.timetable.carriage.exceptions.NoSuchGroupException;
import student.timetable.lessonComponents.Group;

import java.util.Scanner;

public class GroupController extends DirectoryController{

    public String[] readArgument(int a, Scanner sc) {
        int n = 0;
        switch (a) {
            case 1:
            case 2: {
                n = 1;
                break;
            }
            case 3: {
                n = 2;
                break;
            }
            case 4: {
                n = 0;
            }
        }
        String[] resultArray = new String[n];
        for (int i = 0; i < n; i++) {
//            MyView.printGroupHint(a, i);
            resultArray[i] = sc.nextLine();
        }
        return resultArray;
    }

    public void add(String[] argArray) throws GroupAlreadyExistsException {
        Carriage.addGroup(new Group(argArray[0]));
    }

    public void remove(String[] argArray) throws NoSuchGroupException {
        Carriage.removeGroup(new Group(argArray[0]));
    }

    public void edit(String[] argArray) throws NoSuchGroupException {
        Carriage.editGroup(new Group(argArray[0]), argArray[1]);
    }

    public void viewList(String[] argArray) {
//        MyView.printGroupList();
    }
}
