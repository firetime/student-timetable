package student.timetable.controllers;

import student.timetable.carriage.exceptions.IncorrectTimeSlotParametersException;
import student.timetable.carriage.exceptions.NoSuchTimeSlotException;
import student.timetable.lessonComponents.*;
import student.timetable.timetables.TimeTable;

import java.sql.Time;
import java.util.GregorianCalendar;
import java.util.Scanner;

public class LessonController extends GeneralController {

    public void doSmth(int n) throws Exception {
        Scanner sc = new Scanner(System.in);
        switch (n) {
            case 1: {
                add(readArgument(1, sc));
                break;
            }
            case 2: {
                remove(readArgument(2, sc));
                break;
            }
            case 3: {
                edit(readArgument(3, sc));
                break;
            }
            case 4: {
                viewList(readArgument(4, sc));
            }
            default: {
                break;
            }
        }
    }

    public String[] readArgument(int a, Scanner sc) {
        int n = 0;
        switch (a) {
            case 1:
            case 2: {
                n = 11;
                break;
            }
            case 3: {
                n = 19;
                break;
            }
            case 4: {
                n = 0;
            }
        }
        String[] resultArray = new String[n];
        for (int i = 0; i < n; i++) {
//            MyView.printHintLesson(a, i);
            resultArray[i] = sc.nextLine();
        }
        return resultArray;
    }

    public void add(String[] argArray) throws IncorrectTimeSlotParametersException, NoSuchTimeSlotException {
        TimeTable.addLesson(new Subject(argArray[0]), new Teacher(argArray[1]),
                new Audience(argArray[2]), new Group(argArray[3]),
                new TimeSlot(new Time(Integer.parseInt(argArray[4]), Integer.parseInt(argArray[5]), 0),
                        new Time(Integer.parseInt(argArray[6]), Integer.parseInt(argArray[7]), 0)),
                new GregorianCalendar(Integer.parseInt(argArray[8]),
                        Integer.parseInt(argArray[9]), Integer.parseInt(argArray[10])));
    }

    public void remove(String[] argArray) throws IncorrectTimeSlotParametersException, NoSuchTimeSlotException {
        TimeTable.removeLesson(new Subject(argArray[0]), new Teacher(argArray[1]),
                new Audience(argArray[2]), new Group(argArray[3]),
                new TimeSlot(new Time(Integer.parseInt(argArray[4]), Integer.parseInt(argArray[5]), 0),
                        new Time(Integer.parseInt(argArray[6]), Integer.parseInt(argArray[7]), 0)),
                new GregorianCalendar(Integer.parseInt(argArray[8]),
                        Integer.parseInt(argArray[9]), Integer.parseInt(argArray[10])));
    }

    public void add(Subject subject, Teacher teacher, Audience audience,
                    Group group, Time timeStart,
                    Time timeEnd, int year, int month, int day) throws IncorrectTimeSlotParametersException, NoSuchTimeSlotException {
        TimeTable.addLesson(subject, teacher, audience, group,
                new TimeSlot(timeStart, timeEnd), new GregorianCalendar(year, month, day));
    }

    public void remove(Subject subject, Teacher teacher, Audience audience,
                       Group group, Time timeStart,
                       Time timeEnd, int year, int month, int day) throws IncorrectTimeSlotParametersException, NoSuchTimeSlotException {
        TimeTable.removeLesson(subject, teacher, audience, group,
                new TimeSlot(timeStart, timeEnd), new GregorianCalendar(year, month, day));
    }

    public void edit(String[] argArray) throws IncorrectTimeSlotParametersException {
        TimeTable.editLesson(new Subject(argArray[0]), new Teacher(argArray[1]),
                new Audience(argArray[2]), new Group(argArray[3]),
                new TimeSlot(new Time(Integer.parseInt(argArray[4]), Integer.parseInt(argArray[5]), 0),
                        new Time(Integer.parseInt(argArray[6]), Integer.parseInt(argArray[7]), 0)),
                new Subject(argArray[8]), new Teacher(argArray[9]),
                new Audience(argArray[10]), new Group(argArray[11]),
                new TimeSlot(new Time(Integer.parseInt(argArray[12]), Integer.parseInt(argArray[13]), 0),
                        new Time(Integer.parseInt(argArray[14]), Integer.parseInt(argArray[15]), 0)),
                new GregorianCalendar(Integer.parseInt(argArray[16]),
                        Integer.parseInt(argArray[17]), Integer.parseInt(argArray[18])));
    }

    public void edit(Subject subject1, Teacher teacher1, Audience audience1,
                     Group group1, Time timeStart1,
                     Time timeEnd1,
                     Subject subject2, Teacher teacher2, Audience audience2,
                     Group group2, Time timeStart2,
                     Time timeEnd2, int year2, int month2, int day2) throws IncorrectTimeSlotParametersException, NoSuchTimeSlotException {
        TimeTable.editLesson(subject1, teacher1, audience1, group1,
                new TimeSlot(timeStart1, timeEnd1),
                subject2, teacher2, audience2, group2,
                new TimeSlot(timeStart2, timeEnd2), new GregorianCalendar(year2, month2, day2));
    }

    public void viewList(String[] argArray) {
//        MyView.printLessons();
    }
}
