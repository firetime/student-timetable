package student.timetable.controllers;

import student.timetable.carriage.Carriage;
import student.timetable.lessonComponents.Teacher;
import student.timetable.carriage.exceptions.NoSuchTeacherException;
import student.timetable.carriage.exceptions.TeacherAlreadyExistsException;

import java.util.Scanner;

public class TeacherController extends DirectoryController{
    public String[] readArgument(int a, Scanner sc) {
        int n = 0;
        switch (a) {
            case 1:
            case 2: {
                n = 1;
                break;
            }
            case 3: {
                n = 2;
                break;
            }
            case 4: {
                n = 0;
            }
        }
        String[] resultArray = new String[n];
        for (int i = 0; i < n; i++) {
//            MyView.printGroupHint(a, i);
            resultArray[i] = sc.nextLine();
        }
        return resultArray;
    }

    public void add(String[] argArray) throws TeacherAlreadyExistsException {
        Carriage.addTeacher(new Teacher(argArray[0]));
    }

    public void remove(String[] argArray) throws NoSuchTeacherException {
        Carriage.removeTeacher(new Teacher(argArray[0]));
    }

    public void edit(String[] argArray) throws NoSuchTeacherException {
        Carriage.editTeacher(new Teacher(argArray[0]), argArray[1]);
    }

    public void viewList(String[] argArray) {
//        MyView.printTeacherList();
    }
}
