package student.timetable.controllers;

import student.timetable.carriage.Carriage;
import student.timetable.carriage.exceptions.IncorrectTimeSlotParametersException;
import student.timetable.carriage.exceptions.NoSuchTimeSlotException;
import student.timetable.carriage.exceptions.TimeSlotAlreadyExistsException;
import student.timetable.lessonComponents.TimeSlot;

import java.sql.Time;
import java.util.Scanner;

public class TimeSlotController extends DirectoryController{
    public String[] readArgument(int a, Scanner sc) {
        int n = 0;
        switch (a) {
            case 1:
            case 2: {
                n = 4;
                break;
            }
            case 3: {
                n = 8;
                break;
            }
            case 4: {
                n = 0;
            }
        }
        String[] resultArray = new String[n];
        for (int i = 0; i < n; i++) {
//            MyView.printGroupHint(a, i);
            resultArray[i] = sc.nextLine();
        }
        return resultArray;
    }

    public void add(String[] argArray) throws IncorrectTimeSlotParametersException, TimeSlotAlreadyExistsException {
        Carriage.addTimeSlot(new TimeSlot(
                new Time(Integer.parseInt(argArray[0]), Integer.parseInt(argArray[1]), 0),
                new Time(Integer.parseInt(argArray[2]), Integer.parseInt(argArray[3]), 0)));
    }

    public void remove(String[] argArray) throws IncorrectTimeSlotParametersException, NoSuchTimeSlotException {
        Carriage.removeTimeSlot(new TimeSlot(
                new Time(Integer.parseInt(argArray[0]), Integer.parseInt(argArray[1]), 0),
                new Time(Integer.parseInt(argArray[2]), Integer.parseInt(argArray[3]), 0)));
    }

    public void edit(String[] argArray) throws Exception {
        Carriage.editTimeSlot(
                new TimeSlot(
                        new Time(Integer.parseInt(argArray[0]), Integer.parseInt(argArray[1]), 0),
                        new Time(Integer.parseInt(argArray[2]), Integer.parseInt(argArray[3]), 0)),
                new Time(Integer.parseInt(argArray[4]), Integer.parseInt(argArray[5]), 0),
                new Time(Integer.parseInt(argArray[6]), Integer.parseInt(argArray[7]), 0));
    }

    public void viewList(String[] argArray) {
//        MyView.printTimeSlotList();
    }
}
