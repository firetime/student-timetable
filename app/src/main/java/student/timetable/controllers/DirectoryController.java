package student.timetable.controllers;

import java.util.Scanner;

public abstract class DirectoryController extends GeneralController{
    public void doSmth(int n) throws Exception {
        Scanner sc = new Scanner(System.in);
        switch (n) {
            case 1: {
                add(readArgument(1, sc));
                break;
            }
            case 2: {
                remove(readArgument(2, sc));
                break;
            }
            case 3: {
                edit(readArgument(3, sc));
                break;
            }
            case 4: {
                viewList(readArgument(4, sc));
            }
            default: {
                break;
            }
        }
    }

    public abstract String[] readArgument(int a, Scanner sc);
    public abstract void add(String[] argArray) throws Exception;
    public abstract void remove(String[] argArray) throws Exception;
    public abstract void edit(String[] argArray) throws Exception;
    public abstract void viewList(String[] argArray) throws Exception;
}
