package student.timetable.controllers;

import student.timetable.menu.Node;

import java.util.Scanner;

public class Controller {

    public void processRequest(Node firstNode) throws Exception {
        Node node = firstNode;
        Scanner sc = new Scanner(System.in);

//        MyView.printMenu(node);
        String s = sc.nextLine();
        int n = Integer.parseInt(s);
        while (n != node.getChildren().length + 1 && !node.hasParent()) {
            if (n == node.getChildren().length + 1) {
                node = node.goToParent();
            } else if (node.getGeneralController() == null) {
                if (n <= node.getChildren().length) {
                    node = node.goToChild(n);
                } else {
//                    MyView.printErrorBigArgument();
                }
            } else {
                node.doSmth(n);
            }
//            MyView.printMenu(node);
            s = sc.nextLine();
            n = Integer.parseInt(s);
        }
    }
}
