package student.timetable.controllers;

import org.junit.jupiter.api.Test;
import student.timetable.carriage.exceptions.IncorrectTimeSlotParametersException;
import student.timetable.carriage.exceptions.NoSuchLessonException;
import student.timetable.carriage.exceptions.NoSuchTimeSlotException;
import student.timetable.lessonComponents.Audience;
import student.timetable.lessonComponents.Group;
import student.timetable.lessonComponents.Subject;
import student.timetable.lessonComponents.Teacher;
import student.timetable.carriage.exceptions.LessonAlreadyExistsException;
import student.timetable.timetables.DayTimeTable;
import student.timetable.timetables.TimeTable;

import java.sql.Time;
import java.util.GregorianCalendar;

import static org.junit.jupiter.api.Assertions.*;

public class AddLessonControllerTest {
    @Test
    void addLessonControllerTest() throws NoSuchTimeSlotException, IncorrectTimeSlotParametersException {
        int lessonsAmount = DayTimeTable.lessons.size() + 1;
        Subject subject = new Subject("Math");
        Teacher teacher = new Teacher("Alex");
        Audience audience = new Audience("0307");
        Group group = new Group("7.2");
        Time timeStart = new Time(11, 00, 00);
        Time timeEnd = new Time(12, 00, 00);
        int year = 2022;
        int month = 1;
        int day = 27;
//        (new LessonController()).add(subject, teacher, audience, group, timeStart, timeEnd, year, month, day);
//        assertEquals(lessonsAmount, DayTimeTable.lessons.size());
    }

    @Test
    void addLessonControllerExceptionTest() throws NoSuchTimeSlotException, IncorrectTimeSlotParametersException {
        TimeTable timeTable = new TimeTable();
        Subject subject = new Subject("Math");
        Teacher teacher = new Teacher("Alex");
        Audience audience = new Audience("0307");
        Group group = new Group("7.2");
        Time timeStart = new Time(11, 00, 00);
        Time timeEnd = new Time(12, 00, 00);
        int year = 2022;
        int month = 1;
        int day = 27;
//        (new LessonController()).add(subject, teacher, audience, group, timeStart, timeEnd, year, month, day);
//        LessonAlreadyExistsException exception = assertThrows(LessonAlreadyExistsException.class, () ->
//                (new LessonController()).add(subject, teacher, audience, group, timeStart, timeEnd, year, month, day));
//        assertEquals("LessonAlreadyExistsException", exception.getMessage());
    }

    @Test
    void removeLessonControllerTest() throws NoSuchTimeSlotException, IncorrectTimeSlotParametersException {
        TimeTable timeTable = new TimeTable();
        int lessonsAmount = DayTimeTable.lessons.size();
        Subject subject = new Subject("Math");
        Teacher teacher = new Teacher("Alex");
        Audience audience = new Audience("0307");
        Group group = new Group("7.2");
        Time timeStart = new Time(11, 00, 00);
        Time timeEnd = new Time(12, 00, 00);
        int year = 2022;
        int month = 1;
        int day = 27;
//        (new LessonController()).add(subject, teacher, audience, group, timeStart, timeEnd, year, month, day);
//        (new LessonController()).remove(subject, teacher, audience, group, timeStart, timeEnd, year, month, day);
//        assertEquals(lessonsAmount, DayTimeTable.lessons.size());
    }

    @Test
    void removeLessonControllerExceptionTest() {
        TimeTable timeTable = new TimeTable();
        int lessonsAmount = DayTimeTable.lessons.size();
        Subject subject = new Subject("Math");
        Teacher teacher = new Teacher("Alex");
        Audience audience = new Audience("0307");
        Group group = new Group("7.2");
        Time timeStart = new Time(11, 00, 00);
        Time timeEnd = new Time(12, 00, 00);
        int year = 2022;
        int month = 1;
        int day = 27;
//        NoSuchLessonException exception = assertThrows(NoSuchLessonException.class, () ->
//                (new LessonController()).remove(subject, teacher, audience, group, timeStart, timeEnd, year, month, day));
//        assertEquals("NoSuchLessonException", exception.getMessage());
    }

    @Test
    void addDayTimeTableTest() {
        GregorianCalendar gc = new GregorianCalendar(2022, 1, 27);
//        TimeTable.addDayTimeTable(gc);
//        assertTrue(TimeTable.dayTimeTables.contains(gc));
    }
}
