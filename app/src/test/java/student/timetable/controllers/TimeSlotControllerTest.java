package student.timetable.controllers;

import org.junit.jupiter.api.Test;
import student.timetable.carriage.exceptions.IncorrectTimeSlotParametersException;
import student.timetable.carriage.exceptions.NoSuchTimeSlotException;
import student.timetable.carriage.exceptions.TimeSlotAlreadyExistsException;
import student.timetable.carriage.Carriage;

import static org.junit.jupiter.api.Assertions.*;

public class TimeSlotControllerTest {
    @Test
    void addTimeSlotControllerTest() throws TimeSlotAlreadyExistsException, IncorrectTimeSlotParametersException {
        int carr = Carriage.timeslots.size() + 1;
        (new TimeSlotController()).add(new String[]{"11", "00", "12", "00"});
        assertEquals(carr, Carriage.timeslots.size());
    }

    @Test
    void addTimeSlotControllerExceptionTest() throws TimeSlotAlreadyExistsException, IncorrectTimeSlotParametersException {
        (new TimeSlotController()).add(new String[]{"11", "00", "12", "00"});
//        TimeSlotAlreadyExistsException exception = assertThrows(TimeSlotAlreadyExistsException.class, () -> (new TimeSlotController()).add(new String[]{"11", "00", "12", "00"}));
//        assertEquals("TimeSlotAlreadyExistsException", exception.getMessage());
    }

    @Test
    void removeTimeSlotControllerTest() throws TimeSlotAlreadyExistsException, IncorrectTimeSlotParametersException, NoSuchTimeSlotException {
        int carr = Carriage.timeslots.size();
        (new TimeSlotController()).add(new String[]{"11", "00", "12", "00"});
//        (new TimeSlotController()).remove(new String[]{"11", "00", "12", "00"});
//        assertEquals(carr, Carriage.timeslots.size());
    }

    @Test
    void removeTimeSlotControllerExceptionTest() {
        NoSuchTimeSlotException exception = assertThrows(NoSuchTimeSlotException.class, () -> (new TimeSlotController()).remove(new String[]{"11", "00", "12", "00"}));
        assertEquals("NoSuchTimeSlotException", exception.getMessage());
    }

    @Test
    void editTimeSlotControllerTest() throws Exception {
        (new TimeSlotController()).add(new String[]{"11", "00", "12", "00"});
//        (new TimeSlotController()).edit(new String[]{"11", "00", "12", "00", "12", "00", "13", "00"});
        assertFalse(Carriage.teachers.contains(new String[]{"12", "00", "13", "00"}) && !Carriage.teachers.contains(new String[]{"11", "00", "12", "00"}));
    }

    @Test
    void editTimeSlotControllerExceptionTest() {
//        TimeSlotAlreadyExistsException exception = assertThrows(TimeSlotAlreadyExistsException.class, () -> (new TimeSlotController()).edit(new String[]{"11", "00", "12", "00", "12", "00", "13", "00"}));
//        assertEquals("NoSuchTimeSlotException", exception.getMessage());
    }
}
