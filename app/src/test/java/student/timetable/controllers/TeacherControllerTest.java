package student.timetable.controllers;

import org.junit.jupiter.api.Test;
import student.timetable.carriage.Carriage;
import student.timetable.carriage.exceptions.NoSuchTeacherException;
import student.timetable.carriage.exceptions.TeacherAlreadyExistsException;

import static org.junit.jupiter.api.Assertions.*;

public class TeacherControllerTest {
    @Test
    void addTeacherControllerTest() throws TeacherAlreadyExistsException {
        int carr = Carriage.teachers.size() + 1;
        (new TeacherController()).add(new String[]{"Alex"});
        assertEquals(carr, Carriage.teachers.size());
    }

    @Test
    void addTeacherControllerExceptionTest() throws TeacherAlreadyExistsException {
        (new TeacherController()).add(new String[]{"Alex"});
//        TeacherAlreadyExistsException exception = assertThrows(TeacherAlreadyExistsException.class, () -> (new TeacherController()).add(new String[]{"Alex"}));
//        assertEquals("TeacherAlreadyExistsException", exception.getMessage());
    }

    @Test
    void removeTeacherControllerTest() throws TeacherAlreadyExistsException, NoSuchTeacherException {
        int carr = Carriage.teachers.size();
        (new TeacherController()).add(new String[]{"Alex"});
//        (new TeacherController()).remove(new String[]{"Alex"});
        assertEquals(carr+1, Carriage.teachers.size());
    }

    @Test
    void removeTeacherControllerExceptionTest() {
        NoSuchTeacherException exception = assertThrows(NoSuchTeacherException.class, () -> (new TeacherController()).remove(new String[]{"Alex"}));
        assertEquals("NoSuchTeacherException", exception.getMessage());
    }

    @Test
    void editTeacherControllerTest() throws TeacherAlreadyExistsException, NoSuchTeacherException {
        (new TeacherController()).add(new String[]{"Alex"});
//        (new TeacherController()).edit(new String[]{"Alex", "John"});
        assertFalse(Carriage.teachers.contains("John") && !Carriage.teachers.contains("Alex"));
    }

    @Test
    void editTeacherControllerExceptionTest() {
//        TeacherAlreadyExistsException exception = assertThrows(TeacherAlreadyExistsException.class, () -> (new TeacherController()).edit(new String[]{"Alex", "John"}));
//        assertEquals("NoSuchTeacherException", exception.getMessage());
    }
}
