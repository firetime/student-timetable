package student.timetable.controllers;

import org.junit.jupiter.api.Test;
import student.timetable.carriage.exceptions.NoSuchSubjectException;
import student.timetable.carriage.Carriage;
import student.timetable.carriage.exceptions.SubjectAlreadyExistsException;

import static org.junit.jupiter.api.Assertions.*;

public class SubjectControllerTest {
    @Test
    void addSubjectControllerTest() throws SubjectAlreadyExistsException {
        int carr = Carriage.subjects.size() + 1;
        (new SubjectController()).add(new String[]{"Math"});
        assertEquals(carr, Carriage.subjects.size());
    }

    @Test
    void addSubjectControllerExceptionTest() throws SubjectAlreadyExistsException {
        (new SubjectController()).add(new String[]{"Math"});
//        SubjectAlreadyExistsException exception = assertThrows(SubjectAlreadyExistsException.class, () -> (new SubjectController()).add(new String[]{"Math"}));
//        assertEquals("SubjectAlreadyExistsException", exception.getMessage());
    }

    @Test
    void removeSubjectControllerTest() throws SubjectAlreadyExistsException, NoSuchSubjectException {
        int carr = Carriage.subjects.size();
        (new SubjectController()).add(new String[]{"Math"});
//        (new SubjectController()).remove(new String[]{"Math"});
//        assertEquals(carr, Carriage.subjects.size());
    }

    @Test
    void removeSubjectControllerExceptionTest() {
        NoSuchSubjectException exception = assertThrows(NoSuchSubjectException.class, () -> (new SubjectController()).remove(new String[]{"Math"}));
        assertEquals("NoSuchSubjectException", exception.getMessage());
    }

    @Test
    void editSubjectControllerTest() throws SubjectAlreadyExistsException, NoSuchSubjectException {
        (new SubjectController()).add(new String[]{"Math"});
//        (new SubjectController()).edit(new String[]{"Math", "PE"});
//        assertTrue(Carriage.teachers.contains("PE") && !Carriage.teachers.contains("Math"));
    }

    @Test
    void editSubjectControllerExceptionTest() {
//        SubjectAlreadyExistsException exception = assertThrows(SubjectAlreadyExistsException.class, () -> (new SubjectController()).edit(new String[]{"Math", "PE"}));
//        assertEquals("NoSuchSubjectException", exception.getMessage());
    }
}
