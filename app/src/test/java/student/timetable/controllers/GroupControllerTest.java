package student.timetable.controllers;

import org.junit.jupiter.api.Test;
import student.timetable.carriage.Carriage;
import student.timetable.carriage.exceptions.GroupAlreadyExistsException;
import student.timetable.carriage.exceptions.NoSuchGroupException;

import static org.junit.jupiter.api.Assertions.*;

public class GroupControllerTest {
    @Test
    void addGroupControllerTest() throws GroupAlreadyExistsException {
        int carr = Carriage.groups.size() + 1;
        (new GroupController()).add(new String[]{"7.2"});
        assertEquals(carr, Carriage.groups.size());
    }

    @Test
    void addGroupControllerExceptionTest() throws GroupAlreadyExistsException {
        Carriage car = new Carriage();
        GroupController groupController = new GroupController();
        groupController.add(new String[]{"7.2"});
//        GroupAlreadyExistsException exception = assertThrows(GroupAlreadyExistsException.class, () -> groupController.add(new String[]{"7.2"}));
//        assertEquals("GroupAlreadyExistsException", exception.getMessage());
    }


    @Test
    void removeGroupControllerExceptionTest() throws GroupAlreadyExistsException {
        GroupController groupController = new GroupController();
        groupController.add(new String[]{"7.2"});
        NoSuchGroupException exception = assertThrows(NoSuchGroupException.class, () -> (new GroupController()).remove(new String[]{"7.2"}));
        assertEquals("NoSuchGroupException", exception.getMessage());
    }

    @Test
    void editGroupControllerTest() throws GroupAlreadyExistsException, NoSuchGroupException {

//        assertTrue(Carriage.teachers.contains("7.1") && !Carriage.teachers.contains("7.2"));
    }

    @Test
    void editGroupControllerExceptionTest() throws GroupAlreadyExistsException {
        GroupController groupController = new GroupController();
        groupController.add(new String[]{"7.2"});
//        GroupAlreadyExistsException exception = assertThrows(GroupAlreadyExistsException.class, () -> (new GroupController()).edit(new String[]{"7.2", "7.1"}));
//        assertEquals("NoSuchGroupException", exception.getMessage());
    }
}
